# Weatherstack - Test Automation Assignment

[Repository](https://gitlab.com/timlolkema/serenity-assignment) | [Pipelines](https://gitlab.com/timlolkema/serenity-assignment/-/pipelines) |
 
## Table of contents

- [Summary](#summary)
- [Prerequisites](#prerequisites)
- [Project structure](#project-structure)
- [Running tests](#running-tests)
- [Write new tests](#write-new-tests)
- [Reporting](#reporting)
- [GitLab pipeline](#gitlab-pipeline)
 
## Summary

This repository contains the test automation tooling for integration tests of the WeatherStack API's.

The framework is based on the [starter project](https://github.com/serenity-bdd/serenity-rest-starter) provided by Serenity
 
The frameworks used in this repositroy are:
 
>[Serenity](https://serenity-bdd.github.io/)
 
>[Cucumber](https://cucumber.io/)
 
>[Rest Assured](https://rest-assured.io/)
  
### Prerequisites

- Install Java JDK (minimum 8+)
- Install Apache Maven (minimum 3.3.x)
- Install an IDE (e.g. IntelliJ IDEA)

### Project structure

The project is structured based on the different endpoints of the application.

For this project the endpoints `current` and `forecast` are tested.

```
src
 ┣ test
 ┃ ┣ java
 ┃ ┃ ┣ weather
 ┃ ┃ ┃ ┣ current
 ┃ ┃ ┃ ┃ ┣ CurrentWeatherAPI.java                       // contains methods to perform actions on the endpoint
 ┃ ┃ ┃ ┃ ┣ CurrentWeatherResponse.java                  // contains JSON locators of the response from the endpoint         
 ┃ ┃ ┃ ┃ ┗ CurrentWeatherStepDefinitions.java           // contains the step definitions of the endpoint
 ┃ ┃ ┃ ┣ forecast
 ┃ ┃ ┃ ┃ ┣ ForecastWeatherAPI.java
 ┃ ┃ ┃ ┃ ┣ ForecastWeatherResponse.java
 ┃ ┃ ┃ ┃ ┗ ForecastWeatherStepDefinitions.java
 ┃ ┃ ┃ ┣ shared
 ┃ ┃ ┃ ┃ ┗ StepDefinitions.java                         // contains generic step definitions
 ┃ ┃ ┃ ┗ CucumberTestSuite.java                         // contains run options
 ┃ ┣ resources
 ┃ ┃ ┣ features                                         // features are split between endpoints
 ┃ ┃ ┃ ┣ current
 ┃ ┃ ┃ ┃ ┣ current_weather.feature
 ┃ ┃ ┃ ┃ ┗ current_weather_exceptions.feature
 ┃ ┃ ┃ ┣ forecast
 ┃ ┃ ┃ ┃ ┣ forecast_weather.feature
 ┃ ┃ ┃ ┃ ┗ forecast_weather_exceptions.feature
 ┃ ┃ ┣ schemas                                          // schemas contains all JSON schemas
 ┃ ┃ ┃ ┣ current_json_schema.json
 ┃ ┃ ┃ ┗ forecast_json_schema.json
```

### Running tests

- Clone this repository
- `mvn install`
- `mvn clean verify`

### Write new tests

- Write the test as a feature in Gherkin syntax in `src/test/resources/features/<endpoint>`
- Update or create new api actions in `src/test/java/weather/<endpoint>/<endpoint>API`
- Update or create new json locators in `src/test/java/weather/<endpoint>/<endpoint>Response`
- Update or create new step definitions in `src/test/java/weather/<endpoint>/<endpoint>StepDefinitions`

### Reporting

After a `mvn clean verify` a report is generated in `target/site/serenity/index.html`

In this report you can inspect the latest test results.
 
You can view the artifacts within GitLab:
 
`CI/CD > Pipelines > Pipeline ID > Integration tests > Browse`
 
These reports are saved within GitLab for 2 weeks.
 
### GitLab pipeline

In `.gitlab-ci.yml` the GitLab pipeline is defined.

Currently this pipeline will automatically trigger when a merge requests is created.

The merge can only proceed after the pipeline is successful.
