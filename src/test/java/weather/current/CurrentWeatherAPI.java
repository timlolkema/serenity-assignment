package weather.current;

import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;
import net.serenitybdd.core.environment.EnvironmentSpecificConfiguration;
import net.thucydides.core.util.EnvironmentVariables;

/**
 * The CurrentWeatherAPI class provides methods to make requests to the /current endpoint
 */
public class CurrentWeatherAPI {

    EnvironmentVariables env;

    /**
     * Returns the /current endpoint URL
     * @return The /current endpoint
     */
    private String getEndpointUrl(){
        return EnvironmentSpecificConfiguration.from(env)
                .getProperty("api_base_url") + "/current";
    }

    /**
     * Returns the API key defined in the serenity.conf
     * @return The API key
     */
    private String getAPIKey(){
        return EnvironmentSpecificConfiguration.from(env)
                .getProperty("api_key");
    }

    /**
     * Performs a GET request to /current endpoint with the given query as parameter
     * @param  city  The city to add as a query parameter
     */
    @Step("Get current weather by city {0}")
    public void getCurrentWeatherByCity(String city) {
        SerenityRest.given()
                .queryParam("access_key", getAPIKey())
                .queryParam("query", city)
                .get(getEndpointUrl());
    }

    /**
     * Performs a GET request to /current endpoint with the given query and access_key
     * @param  city       The city to add as a query parameter
     * @param  accessKey  The access_key to add as a query parameter
     */
    @Step("Get current weather with invalid access_key")
    public void getCurrentWeatherWithInvalidAccessKey(String city, String accessKey) {
        SerenityRest.given()
                .queryParam("access_key", accessKey)
                .queryParam("query", city)
                .get(getEndpointUrl());
    }
}
