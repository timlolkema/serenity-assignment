package weather.current;

/**
 * The CurrentWeatherResponse class provides JSON locators for the /current response
 */
public class CurrentWeatherResponse {
    public static final String COUNTRY = "'location'.'country'";
    public static final String ERROR_CODE = "'error'.'code'";
}
