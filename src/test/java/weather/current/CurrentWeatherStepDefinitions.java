package weather.current;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;

import static net.serenitybdd.rest.SerenityRest.restAssuredThat;
import static org.hamcrest.Matchers.equalTo;

/**
 * The CurrentWeatherStepDefinitions class provides the step definitions to be used in the feature files
 */
public class CurrentWeatherStepDefinitions {
    @Steps
    CurrentWeatherAPI currentWeatherAPI;

    @When("I make a GET request to the current endpoint for {string}")
    public void requestCurrentWeatherFor(String city) {
        currentWeatherAPI.getCurrentWeatherByCity(city);
    }

    @When("I make a GET request to the current endpoint for {string} with access_key {string}")
    public void requestCurrentWeatherWithInvalidAccessKey(String city, String accessKey) {
        currentWeatherAPI.getCurrentWeatherWithInvalidAccessKey(city, accessKey);
    }

    @Then("The response of current should return the weather for country {string}")
    public void theResponseShouldIncludeCountry(String country) {
        restAssuredThat(response -> response.body(CurrentWeatherResponse.COUNTRY, equalTo(country)));
    }

    @Then("The response of current should contain error code {int}")
    public void theResponseShouldContainError(Integer errorCode) {
        restAssuredThat(response -> response.body(CurrentWeatherResponse.ERROR_CODE, equalTo(errorCode)));
    }
}
