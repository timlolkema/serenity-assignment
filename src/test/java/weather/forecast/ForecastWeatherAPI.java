package weather.forecast;

import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;
import net.serenitybdd.core.environment.EnvironmentSpecificConfiguration;
import net.thucydides.core.util.EnvironmentVariables;

public class ForecastWeatherAPI {

    EnvironmentVariables env;

    /**
     * Returns the /forecast endpoint URL
     * @return The /forecast endpoint
     */
    private String getEndpointUrl(){
        return EnvironmentSpecificConfiguration.from(env)
                .getProperty("api_base_url") + "/forecast";
    }

    /**
     * Returns the API key defined in the serenity.conf
     * @return The API key
     */
    private String getAPIKey(){
        return EnvironmentSpecificConfiguration.from(env)
                .getProperty("api_key");
    }

    /**
     * Performs a GET request to /forecast endpoint with the given query as parameter
     * @param  city  The city to add as a query parameter
     */
    @Step("Get forecast weather by city {0}")
    public void getForecastWeatherByCity(String city) {
        SerenityRest.given()
                .queryParam("access_key", getAPIKey())
                .queryParam("query", city)
                .get(getEndpointUrl());
    }

    /**
     * Performs a GET request to /forecast endpoint with the given query as parameter
     * @param  city  The city to add as a query parameter
     * @param  forecastDays  The amount of days to request the forecast for (this will only work on a paid plan of the API)
     */
    @Step("Get forecast weather by city {0} for days {1}")
    public void getForecastWeatherByCityForDays(String city, String forecastDays) {
        SerenityRest.given()
                .queryParam("access_key", getAPIKey())
                .queryParam("forecast_days", forecastDays)
                .queryParam("query", city)
                .get(getEndpointUrl());
    }

    /**
     * Performs a GET request to /forecast endpoint with the given query and access_key
     * @param  city       The city to add as a query parameter
     * @param  accessKey  The access_key to add as a query parameter
     */
    @Step("Get forecast weather with invalid access_key")
    public void getForecastWeatherWithInvalidAccessKey(String city, String accessKey) {
        SerenityRest.given()
                .queryParam("access_key", accessKey)
                .queryParam("query", city)
                .get(getEndpointUrl());
    }
}
