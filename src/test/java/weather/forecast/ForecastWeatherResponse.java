package weather.forecast;

/**
 * The ForecastWeatherResponse class provides JSON locators for the /forecast response
 */
public class ForecastWeatherResponse {
    public static final String ERROR_CODE = "'error'.'code'";
    public static final String FORECAST = "'forecast'";
}
