package weather.forecast;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;

import static net.serenitybdd.rest.SerenityRest.restAssuredThat;
import static org.hamcrest.Matchers.equalTo;

/**
 * The ForecastWeatherStepDefinitions class provides the step definitions to be used in the feature files
 */
public class ForecastWeatherStepDefinitions {
    @Steps
    ForecastWeatherAPI forecastWeatherAPI;

    @When("I make a GET request to the forecast endpoint for {string}")
    public void requestForecastWeatherFor(String city) {
        forecastWeatherAPI.getForecastWeatherByCity(city);
    }

    @When("I make a GET request to the forecast endpoint for {string} for {string} days")
    public void requestForecastWeatherFor(String city, String forecastDays) {
        forecastWeatherAPI.getForecastWeatherByCityForDays(city, forecastDays);
    }

    @When("I make a GET request to the forecast endpoint for {string} with access_key {string}")
    public void requestForecastWeatherWithInvalidAccessKey(String city, String accessKey) {
        forecastWeatherAPI.getForecastWeatherWithInvalidAccessKey(city, accessKey);
    }

    @Then("The response of forecast should contain error code {int}")
    public void theResponseShouldContainError(Integer errorCode) {
        restAssuredThat(response -> response.body(ForecastWeatherResponse.ERROR_CODE, equalTo(errorCode)));
    }
}
