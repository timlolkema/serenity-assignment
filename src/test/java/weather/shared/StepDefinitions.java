package weather.shared;

import io.cucumber.java.en.Then;

import static net.serenitybdd.rest.SerenityRest.restAssuredThat;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;

/**
 * This StepDefinitions class provides shared step definitions to be used in the feature files
 */
public class StepDefinitions {
    @Then("The status code should be {int}")
    public void theStatusCodeShouldBe(Integer status) {
        restAssuredThat(response -> response.statusCode(status));
    }

    @Then("The response validates to JSON shema {string}")
    public void theResponseValidatesJSONSchema(String schemaName) {
        restAssuredThat(response -> response.body(matchesJsonSchemaInClasspath(schemaName)));
    }
}
