Feature: Tests for the /current endpoint

  Scenario: GET /current should return a 200
    When I make a GET request to the current endpoint for "Amsterdam"
    Then The status code should be 200

  Scenario: GET /current response should validate to JSON schema "current_json_schema.json"
    When I make a GET request to the current endpoint for "Amsterdam"
    Then The response validates to JSON shema "schemas/current_json_schema.json"

  Scenario: GET /current should return a the correct country in the response
    When I make a GET request to the current endpoint for "Amsterdam"
    Then The response of current should return the weather for country "Netherlands"