Feature: Tests for checking various exception scenarios for the /current endpoint

  Scenario: GET /current response should return error code 101 if there is an invalid access key
    When I make a GET request to the current endpoint for "Amsterdam" with access_key "invalid"
    Then The response of current should contain error code 101

  Scenario Outline: GET /current should return correct error code for invalid queries
    When I make a GET request to the current endpoint for <query>
    Then The response of current should contain error code <errorCode>
    Examples:
      | query       | errorCode   |
      | "A"         | 615         |
      | "0"         | 601         |
