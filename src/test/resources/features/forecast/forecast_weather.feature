Feature: Tests for the /forecast endpoint

  Scenario: GET /forecast should return a 200
    When I make a GET request to the forecast endpoint for "Amsterdam"
    Then The status code should be 200

  Scenario: GET /forecast response should validate to JSON schema "forecast_json_schema.json"
    When I make a GET request to the forecast endpoint for "Amsterdam"
    Then The response validates to JSON shema "schemas/forecast_json_schema.json"
