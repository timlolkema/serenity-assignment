Feature: Tests for checking various exception scenarios for the /forecast endpoint

  Scenario:  GET /forecast response should return error code 609 when forecast_days is not in plan
    When I make a GET request to the forecast endpoint for "Amsterdam" for "2" days
    Then The response of current should contain error code 609
